# README #


### What is this repository for? ###

A simple meteor app with login and message functionality. User can login and add messages and delete own messages.

### How do I get set up? ###
meteor run


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Pravar Kulkarni (pravar.k@cisinlabs.com)